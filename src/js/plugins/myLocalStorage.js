function updateLocal(array) {
  if (localStorage.getItem("items")) {
    localStorage.removeItem("items");
  }
  if (!array || !array.length) {
    return;
  }
  localStorage.setItem("items", JSON.stringify(array));
}

function loadFromLocal() {
  let localArray = localStorage.getItem("items");

  if (!localArray) return [];

  localArray = JSON.parse(localArray);

  return localArray;
}

function updateResponseLocal(array) {
  if (!array || !array.length) {
    return;
  }
  localStorage.setItem("response-items", JSON.stringify(array));
}

export { updateLocal, loadFromLocal, updateResponseLocal };
