import { updateLocal, loadFromLocal } from "../plugins/myLocalStorage";

class Favorites {
  constructor() {
    this.favoritesTickets = loadFromLocal();
  }

  addFavoriteTickets(object, elem) {
    if (this.favoritesTickets.includes(object)) {
      return;
    }
    this.favoritesTickets.push(object);
    updateLocal(this.favoritesTickets);
    this.addSumTickets(elem);
  }

  removeFavoriteTickets(object, elem) {
    this.favoritesTickets = this.favoritesTickets.filter((item) => {
      return item.id !== object.id;
    });
    this.addSumTickets(elem);
    updateLocal(this.favoritesTickets);
  }

  addSumTickets(elem) {
    elem.setAttribute("data-count", this.favoritesTickets.length);
    if (this.favoritesTickets.length === 0) {
      elem.removeAttribute("data-count");
    }
  }
}

const favorites = new Favorites();

export default favorites;
