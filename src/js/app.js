import "../css/style.css";
import "./plugins";
import locations from "./store/locations";
import favorites from "./store/favorites";

import formUI from "./views/form";
import ticketsUI from "./views/tickets";
import currencyUI from "./views/currency";
import ticketFavoritesUI from "./views/ticket-favorites";

document.addEventListener("DOMContentLoaded", (e) => {
  const form = formUI.form;
  const containerTickets = ticketsUI.containerTickets;
  const containerFavoritesTickets = ticketFavoritesUI.containerTickets;
  const btnShowFavoritesTickets = ticketFavoritesUI.btnShowMyTickets;
  const loader = document.querySelector(".preloader");

  //Loader
  loader.classList.add("active");

  // Events
  initApp();
  form.addEventListener("submit", (e) => {
    e.preventDefault();
    onFormSubmit();
  });

  containerFavoritesTickets.addEventListener("click", deleteTickets);
  containerTickets.addEventListener("click", addTickets);

  // handlers

  function addTickets(e) {
    e.preventDefault();
    const targ = e.target;

    if (!targ.classList.contains("js-add")) {
      return;
    }
    const card = targ.closest(".ticket-card");
    const cardId = card.getAttribute("data-id");
    const ticketItem = locations.lastSearch.find((item) => {
      return item.id == cardId;
    });

    card.classList.add("like");
    ticketItem.currency = card.querySelector(".ticket-price").textContent;
    favorites.addFavoriteTickets(ticketItem, btnShowFavoritesTickets);
    ticketFavoritesUI.renderFavoriteTickets(favorites.favoritesTickets);
  }

  function deleteTickets(e) {
    e.preventDefault();
    const targ = e.target;
    if (!targ.classList.contains("delete-favorite")) {
      return;
    }

    const cardId = targ.closest(".favorite-item").getAttribute("data-id");
    const ticketItem = favorites.favoritesTickets.find((item) => {
      return item.id == cardId;
    });

    const activeCard = containerTickets.querySelector(`[data-id="${cardId}"]`);

    if (activeCard) {
      activeCard.classList.remove("like");
    }

    favorites.removeFavoriteTickets(ticketItem, btnShowFavoritesTickets);
    ticketFavoritesUI.renderFavoriteTickets(favorites.favoritesTickets);
  }

  async function initApp() {
    if (favorites.favoritesTickets) {
      ticketFavoritesUI.renderFavoriteTickets(favorites.favoritesTickets);
      favorites.addSumTickets(btnShowFavoritesTickets);
    }
    let response = await locations.init();
    if (response) {
      loader.classList.remove("active");
    }
    formUI.setAutocompleteData(locations.shortCities);
  }

  async function onFormSubmit() {
    const origin = locations.getCityCodeByKey(formUI.originValue);
    const destination = locations.getCityCodeByKey(formUI.destinationValue);
    const depart_date = formUI.departDateValue;
    const return_date = formUI.returnDateValue;
    const currency = currencyUI.currecyValue;

    await locations.fetchTickets({
      origin,
      destination,
      depart_date,
      return_date,
      currency,
    });

    ticketsUI.renderTickets(locations.lastSearch);
  }
});
